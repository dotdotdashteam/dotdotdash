﻿using UnityEngine;
using System.Collections;

public class InputHandler : MonoBehaviour {

    //The key to use, stored here for easier access.
    KeyCode inputKey = KeyCode.UpArrow;

    //If the button is being pressed.
    bool inputActive;

    //Are we inside anything? What is it?
    bool insideDot, insideDash;

    //Was input too early? Is it too late to start pressing the button? Have we held the button long enough?
    bool startedEarly, lateStartPast, minimumLengthReached;

    //The current platform is already enabled.
    bool currentPlatformEnabled;
    
    Platform currentPlatform;

    //This could be handled better, but the project is pretty small.
    public PlatformManager platformManager;
    public Player player;

	// Use this for initialization
	void Start () {
	    
	}
	
	// Update is called once per frame
	void Update () {
        //Get the input.
        inputActive = Input.GetKey(inputKey);

        //Cause the player to stumble if input is pressed when it shouldn't be.
        if (inputActive && !insideDot && !insideDash)
        {
            startedEarly = true;
            //TODO: Player.Stumble(); //Or something along these lines.
        }
        else if (!inputActive && insideDash && !minimumLengthReached)
        {
            //If input isn't active, but we are inside a dash that hasn't reached minimum length.
            currentPlatform.Disable();
        }
        
        //If we're within the alloted start margin.
        if(!startedEarly && !lateStartPast && !currentPlatformEnabled)
        {
            //If the key is being pressed.
            if (inputActive)
            {
                //Activate this platform.
                currentPlatform.Enable();
                currentPlatformEnabled = true;
            }
        }
    }

    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.CompareTag("Platform_Dash"))
        {
            lateStartPast = false;

            insideDot = false;
            insideDash = true;

            minimumLengthReached = false;
            currentPlatform = other.GetComponent<Platform>();

            currentPlatformEnabled = false;
        }
        else if (other.CompareTag("Platform_Dot"))
        {
            lateStartPast = false;

            insideDot = true;
            insideDash = false;

            minimumLengthReached = false;
            currentPlatform = other.GetComponent<Platform>();

            currentPlatformEnabled = false;
        }
        else if (other.CompareTag("Platform_Dash_Inner") || other.CompareTag("Platform_Dot_Inner"))
        {
            //The opportunity to hit this button is over.
            lateStartPast = true;
        }
    }

    void OnTriggerExit2D(Collider2D other)
    {
        //Collisions with platform exteriors and interiors.
        if (other.CompareTag("Platform_Dot") || other.CompareTag("Platform_Dash"))
        {
            insideDash = false;
            insideDot = false;

            //Destroy this platform, the key was held too long.
            if (inputActive)
            {
                currentPlatform.Disable();
            }

            //Indicate that we've left the platform that was started early. Useful for debugging and 'zen' mode.
            startedEarly = false;
        }
        else if (other.CompareTag("Platform_Dash_Inner") || other.CompareTag("Platform_Dot_Inner"))
        {
            minimumLengthReached = true;
        }
    }
}
