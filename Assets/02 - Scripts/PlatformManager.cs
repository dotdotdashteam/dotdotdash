﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

/**
 * Controls and maintains all platforms on screen.
 * Instantiates new platforms using the prefabs for letters/number (aka Platform Sets).
 */
public class PlatformManager : MonoBehaviour {
    //TODO: Might be best to make this a singleton
    // and have all platforms call CreateNextPlatform as they destroy themselves. 
    public float LetterSpace, WordSpace;

    private char nextPlatformCharacter;
    private Vector3 nextPlatformPosition;

    //Maintain a list of live platforms.
    //This will include all platforms currently on screen, as well as 2-3 platform sets to the left and 4-5 platform sets to the right.
    private List<Platform> livePlatformSets;

    public float speed = 8f;

    public FileReader myFileReader;

    //An array of prefabs to use for chars.
    public GameObject[] morseChars;

    public int PlatformCount { get; set; }

	// Use this for initialization
	void Start () {
        nextPlatformPosition = new Vector3(0, 0, 0);

        //Create the first 10 platforms.
        for(int i = 0; i < 10; i++)
        {
            CreateNextMorseChar();
        }
    }
	
	// Update is called once per frame
	void Update () {
        //Move the platforms across the screen.
        transform.Translate(Vector3.left * Time.deltaTime * speed);

        //Always have a minimum of 10 live platforms.
        if(PlatformCount < 10)
        {
            CreateNextMorseChar();
        }
    }

    /**
     * Creates a platform of the next character at the appropriate location.
     */
    public void CreateNextMorseChar()
    {
        //Get the next platform char.
        nextPlatformCharacter = myFileReader.GetNextChar();

        //TODO: Handle special cases like " " and punctuation by moving the nextLocation vector.
        switch (nextPlatformCharacter)
        {
            //Space.
            case ' ':
                //The letter space has already been added, so we want to add only the difference for words.
                nextPlatformPosition += Vector3.right * (WordSpace - LetterSpace);
                return;
            case '.':
            case ',':
            case ':':
            case '!':
            case '?':
                //Add an additional LetterSpace, which will in turn be subtracted assuming the next character is a ' '.
                nextPlatformPosition += Vector3.right * (LetterSpace);
                return;
            //TODO: Handle all other cases that aren't letters. Better approach may be to check if char
            //      is letter, ' ', or other instead of using a switch.
        }

        //TODO: Recycle a platform if possible, create a new one otherwise.
        //(int)(nextPlatformCharacter % 32) is a fast way to get the 1-indexed position of a char regardless of case.
        GameObject newPlatform  = (GameObject)Instantiate(morseChars[(int)(nextPlatformCharacter % 32) - 1], transform);
        newPlatform.transform.localPosition = nextPlatformPosition;
        
        //Add the letter width and a buffer letter space;
        nextPlatformPosition += Vector3.right * (newPlatform.GetComponent<MorseChar>().GetWidth() + LetterSpace);
        PlatformCount++;
    }
}
