﻿using UnityEngine;
using System.Collections;

public class MorseChar : MonoBehaviour {
    //The width of a dot or a dash.
    static float dotWidth = .7f;
    static float dashWidth = 2.1f;
    //The amount of space between dots and dashes.
    static float buffer = .55f;

    private float width;
    
    public int dots, dashes;

    Platform[] platforms;
    
    // Use this for initialization
    void Awake () {
        //Set the width to the width of all dots and dashes + the space between them.
        width = (dots * dotWidth) + (dashes * dashWidth) + (buffer * (dots + dashes - 1));

        platforms = GetComponentsInChildren<Platform>();
        
        for (int i = 0; i < platforms.Length; i++)
        {
            //Set platforms to active, this is only relevant in a recycled morse char.
            platforms[i].gameObject.SetActive(true);

            //Set this object as the platform's parent.
            platforms[i].SetParent(this);
        }
    }
	
	// Update is called once per frame
	void Update () {
	
	}

    public float GetWidth()
    {
        return width;
    }

    public void Recycle()
    {
        //Reduce the count of platforms (MorseChars)
        //TODO: FindObjectOfType is slow, but will be fine for now.
        FindObjectOfType<PlatformManager>().PlatformCount--;

        //TODO: Recycle this char instead of destroying.
        Destroy(gameObject);
    }
}
