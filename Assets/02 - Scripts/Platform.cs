﻿using UnityEngine;
using System.Collections;

public class Platform : MonoBehaviour {

    MorseChar parent;

    //The individual displayed portions of the platform.
    public GameObject[] parts;

    // Use this for initialization
    void Start()
    {

    }
    
    /**
     * Turn the platforms on, allowing them to be run on.
     */
    public void Enable()
    {
        //Loop to activate the parts.
        for (int i = 0; i < parts.Length; i++)
        {
            parts[i].SetActive(true);
        }
    }

    /**
     * Turn the platforms off.
     */
    public void Disable()
    {
        //Loop to deactivate the parts.
        for (int i = 0; i < parts.Length; i++)
        {
            parts[i].SetActive(false);
        }
    }

    public void SetParent(MorseChar target)
    {
        parent = target;
    }

    public void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject.CompareTag("Destroyer"))
        {
            parent.Recycle();
        }
    }
}
