﻿using UnityEngine;
using System;
using System.Collections;
using System.Text;
using System.IO;

public class FileReader : MonoBehaviour {

    String fileText;
    int position = 0;

	// Use this for initialization
	void Awake () {
        fileText = "";
        Load("Assets/09 - Text/test.txt");
	}
	
    public char GetNextChar()
    {
        if(position < fileText.Length)
        {
            //Increment the position.
            position++;

            //Get the string at the pre-incremented position.
            return fileText[position - 1];
        }

        //Just return 'a'. We'll figure out what to do at the end of the file later.
        return 'a';
    }

    private bool Load(string fileName)
    {
        // Handle any problems that might arise when reading the text
        try
        {
            string line;
            // Create a new StreamReader.
            StreamReader reader = new StreamReader(fileName, Encoding.Default);
            // Immediately clean up the reader after this block of code is done.
            // You generally use the "using" statement for potentially memory-intensive objects
            // instead of relying on garbage collection.
            // (Do not confuse this with the using directive for namespace at the 
            // beginning of a class!)
            using (reader)
            {
                // While there's lines left in the text file, do this:
                do
                {
                    line = reader.ReadLine();

                    if (line != null)
                    {
                        //Add the line to our char array.
                        fileText += " "  + line;
                    }
                }
                while (line != null);

                // Done reading, close the reader and return true to broadcast success    
                reader.Close();
                return true;
            }
        }
        // If anything broke in the try block, we throw an exception with information
        // on what didn't work
        catch (Exception e)
        {
            Debug.Log(e.Message);
            //Console.WriteLine("{0}\n", e.Message);
            return false;
        }
    }
}
